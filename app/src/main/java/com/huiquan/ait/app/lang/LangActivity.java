package com.huiquan.ait.app.lang;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.huiquan.ait.app.R;
import org.greenrobot.eventbus.EventBus;


/**
 * Created by Administrator on 2017/11/12.
 */

public class LangActivity extends AppCompatActivity {
    private Toolbar toolBar;
    private ListView langView;
    private boolean direction;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lang_view);
        initGet();
        initView();
    }

    private void initGet() {
        Intent intent = getIntent();
        direction = intent.getBooleanExtra("direction", true);
    }

    private void initView() {
        langView = (ListView) findViewById(R.id.lang_view);
        toolBar = (Toolbar) findViewById(R.id.toolbar);
        toolBar.setNavigationIcon(R.mipmap.return_png);
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            //返回键监听
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        langView.setAdapter(new ArrayAdapter<Lang>(this, android.R.layout.simple_list_item_1, Lang.values()));
        langView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                EventBus.getDefault().post(new LangMessage(Lang.values()[i], direction));
                finish();
            }
        });
    }

}
