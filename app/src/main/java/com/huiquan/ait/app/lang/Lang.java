package com.huiquan.ait.app.lang;

/**
 * Created by soy50 on 2017/11/15.
 */
public enum Lang {
    China(1536, "中文", "zh"), English(1736, "英语", "en"), Cantonese(1636, "粤语", "");
    
    private String name;
    private String code;
    private int pid;

    private Lang(int pid, String name, String code) {
        this.pid = pid;
        this.name = name;
        this.code = code;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
