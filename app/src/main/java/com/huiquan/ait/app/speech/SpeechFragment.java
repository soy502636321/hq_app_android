package com.huiquan.ait.app.speech;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import android.widget.PopupWindow;

import com.baidu.speech.EventListener;
import com.baidu.speech.EventManager;
import com.baidu.speech.EventManagerFactory;
import com.baidu.speech.asr.SpeechConstant;
import com.google.common.base.Strings;
import com.huiquan.ait.app.MainActivity;
import com.huiquan.ait.app.R;

import com.huiquan.ait.app.text.Example;
import okhttp3.*;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SpeechFragment extends Fragment implements EventListener {

    private FloatingActionButton speechSrcFab;
    private FloatingActionButton speechTgtFab;
    private ListView chatView;
    private EventManager asr;
    private String result;
    private List<Chat> chats = new ArrayList<>();
    private ChatAdapter chatAdapter;

    private PopupWindow popupWindow;
    private Dialog loadingDialog;
    private boolean speeching = true;

    public static SpeechFragment newInstance() {
        SpeechFragment fragment = new SpeechFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_speech, container, false);
        initPermission();
        initAsr();
        initView(view);
        initSpeechPopup(inflater);
        return view;
    }

    @Override
    public void onEvent(String name, String params, byte[] data, int offset, int length) {
        if (SpeechConstant.CALLBACK_EVENT_ASR_PARTIAL.equals(name)) {
            try {
                JSONObject object = new JSONObject(params);
                setResult(object.getString("best_result"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (SpeechConstant.CALLBACK_EVENT_ASR_FINISH.equals(name)) {
            translator(getResult());
        }
    }

    public void startSpeech(boolean speeching) {
        setResult("");
        this.speeching = speeching;
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME, false);
        params.put(SpeechConstant.VAD, SpeechConstant.VAD_DNN);
        params.put(SpeechConstant.PID, speeching ? MainActivity.srcLang.getPid() : MainActivity.tgtLang.getPid());
        params.put(SpeechConstant.DECODER, 0);
        asr.send(SpeechConstant.ASR_START, new JSONObject(params).toString(), null, 0, 0);
    }

    public void stopSpeech() {
        asr.send(SpeechConstant.ASR_STOP, null, null, 0, 0);
    }

    private void initPermission() {
        String permissions[] = {Manifest.permission.RECORD_AUDIO,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        ArrayList<String> toApplyList = new ArrayList<String>();

        for (String perm : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(getContext(), perm)) {
                toApplyList.add(perm);
                //进入到这里代表没有权限.
            }
        }
        String tmpList[] = new String[toApplyList.size()];
        if (!toApplyList.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), toApplyList.toArray(tmpList), 123);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // 此处为android 6.0以上动态授权的回调，用户自行实现。
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public ChatAdapter getChatAdapter() {
        return chatAdapter;
    }

    public List<Chat> getChats() {
        return chats;
    }

    private void initView(View view) {
        speechSrcFab = view.findViewById(R.id.speech_src_fab); //原文语音按钮
        speechTgtFab = view.findViewById(R.id.speech_tgt_fab); //译文语音按钮
        chatView = view.findViewById(R.id.chat_view);
        chatAdapter = new ChatAdapter(getContext(), chats);
        chatView.setAdapter(chatAdapter);

        speechSrcFab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                popupWindow.showAtLocation(getView(), Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                startSpeech(true); //开始识别
                return false;
            }
        });

        speechSrcFab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        //判断是否是长按后执行的操作
                        if (popupWindow.isShowing()) {
                            stopSpeech(); //停止识别
                            popupWindow.dismiss();  //语言提示隐藏
                        }
                        break;
                }
                return false;
            }
        });

        speechTgtFab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //后面可以做语音接收处理
                popupWindow.showAtLocation(getView(), Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
                startSpeech(false); //开始识别
                return false;
            }
        });

        speechTgtFab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        //判断是否是长按后执行的操作
                        if (popupWindow.isShowing()) {
                            stopSpeech(); //停止识别
                            popupWindow.dismiss();  //语言提示隐藏
                        }
                        break;
                }
                return false;
            }
        });
    }

    //样例监听
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onChatEvent(Chat chat) {
        getChats().add(chat);
        getChatAdapter().notifyDataSetChanged();
    }

    private void initSpeechPopup(LayoutInflater inflater) {
        View contentview = inflater.inflate(R.layout.speech_popup, null);
        contentview.setFocusable(true);
        contentview.setFocusableInTouchMode(true);
        popupWindow = new PopupWindow(contentview, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(false);    //点击不消失
        popupWindow.setOutsideTouchable(false);
    }

    private void initAsr() {
        asr = EventManagerFactory.create(getContext(), "asr");
        asr.registerListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public boolean isSpeeching() {
        return speeching;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void translator(final String text) {
        OkHttpClient okHttpClient = new OkHttpClient();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("src", isSpeeching() ? MainActivity.srcLang.getCode() : MainActivity.tgtLang.getCode());
        params.put("tgt", isSpeeching() ? MainActivity.tgtLang.getCode() : MainActivity.srcLang.getCode());
        params.put("text", text);
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), new JSONObject(params).toString());
        Request request = new Request.Builder().url("http://ait.huiquan.com/app/api/translator")
                .post(body)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    String result = json.getString("result");
                    if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(result)) {
                        EventBus.getDefault().post(new Chat(text, result, isSpeeching()));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
