package com.huiquan.ait.app.text;

/**
 * Created by soy50 on 2017/11/15.
 */
public class Translator {
    private String id;
    private String srcText;
    private String tgtText;

    public Translator() {

    }

    public Translator(String srcText, String tgtText) {
        this.srcText = srcText;
        this.tgtText = tgtText;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setSrcText(String srcText) {
        this.srcText = srcText;
    }

    public String getSrcText() {
        return srcText;
    }

    public void setTgtText(String tgtText) {
        this.tgtText = tgtText;
    }

    public String getTgtText() {
        return tgtText;
    }
}
