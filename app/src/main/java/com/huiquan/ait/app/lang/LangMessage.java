package com.huiquan.ait.app.lang;

/**
 * Created by Administrator on 2017/11/12.
 */

public class LangMessage {
    private Lang lang;
    private boolean direction;

    public LangMessage(Lang lang, boolean direction) {
        this.lang = lang;
        this.direction = direction;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    public Lang getLang() {
        return lang;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public boolean isDirection() {
        return direction;
    }
}
