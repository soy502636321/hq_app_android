package com.huiquan.ait.app.speech;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import com.huiquan.ait.app.R;

import java.util.List;

/**
 * Created by soy50 on 2017/11/12.
 */
public class ChatAdapter extends BaseAdapter {
    private List<Chat> chats;
    private LayoutInflater inflater;

    public ChatAdapter(Context context, List<Chat> chats) {
        this.chats = chats;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return chats != null ? chats.size() : 0;
    }

    @Override
    public Chat getItem(int position) {
        return (chats != null && position <= chats.size()) ? chats.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Chat chat = getItem(position);
        View view = inflater.inflate(chat.isDirection() ? R.layout.chat_item_src : R.layout.chat_item_tgt, null);
        EditText chatItemSrcText = view.findViewById(R.id.chat_item_src_text);
        TextView chatItemTgtText = view.findViewById(R.id.chat_item_tgt_text);
        chatItemSrcText.setText(chat.getSrcText());
        chatItemTgtText.setText(chat.getTgtText());
        return view;
    }
}
