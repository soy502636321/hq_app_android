package com.huiquan.ait.app;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huiquan.ait.app.lang.Lang;
import com.huiquan.ait.app.lang.LangMessage;
import com.huiquan.ait.app.lang.LangActivity;
import com.huiquan.ait.app.speech.SpeechFragment;
import com.huiquan.ait.app.text.TextFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Map<String, Fragment> fragmentMap = new HashMap<>();
    private TextView srcLangText;
    private TextView tgtLangText;
    public static Lang srcLang = Lang.China;
    public static Lang tgtLang = Lang.English;
    private ImageView replaceImg;
    private LinearLayout leftLiner;
    private LinearLayout rightLiner;

    private LinearLayout wordLiner;
    private LinearLayout speechLiner;
    private ImageView wordIcon;
    private ImageView speeehIcon;
    private TextView wordText;
    private TextView speechText;

    private List<Fragment> fragments = new ArrayList<>();
    private FragmentStatePagerAdapter fragmentStatePagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        fragmentMap.put("文本", TextFragment.newInstance());
//        fragmentMap.put("语音", SpeechFragment.newInstance());
//
//        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        leftLiner = (LinearLayout) findViewById(R.id.left_liner);
        rightLiner = (LinearLayout) findViewById(R.id.right_liner);
        replaceImg = (ImageView) findViewById(R.id.replaceImg);
        srcLangText = (TextView) findViewById(R.id.src_lang_text);
        tgtLangText = (TextView) findViewById(R.id.tgt_lang_text);
        srcLangText.setText(srcLang.toString());
        tgtLangText.setText(tgtLang.toString());

//        viewPager.setAdapter(new AppFragmentPagerAdapter(getSupportFragmentManager(), fragmentMap));
//        tabLayout.setupWithViewPager(viewPager);
        initView1();
        init();
        initView();
    }

    private void initView1() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        leftLiner = (LinearLayout) findViewById(R.id.left_liner);
        rightLiner = (LinearLayout) findViewById(R.id.right_liner);
        replaceImg = (ImageView) findViewById(R.id.replaceImg);
        srcLangText = (TextView) findViewById(R.id.src_lang_text);
        tgtLangText = (TextView) findViewById(R.id.tgt_lang_text);

        wordLiner = (LinearLayout) findViewById(R.id.word_liner);
        speechLiner = (LinearLayout) findViewById(R.id.speech_liner);
        wordIcon = (ImageView) findViewById(R.id.word_icon);
        speeehIcon = (ImageView) findViewById(R.id.speeeh_icon);
        wordText = (TextView) findViewById(R.id.word_text);
        speechText = (TextView) findViewById(R.id.speech_text);

        srcLangText.setText(srcLang.toString());
        tgtLangText.setText(tgtLang.toString());


    }


    private void initView() {
        leftLiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent src_lang_view = new Intent(MainActivity.this, LangActivity.class);
                src_lang_view.putExtra("direction", true);
                startActivity(src_lang_view);
            }
        });
        rightLiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tgt_lang_view = new Intent(MainActivity.this, LangActivity.class);
                tgt_lang_view.putExtra("direction", false);
                startActivity(tgt_lang_view);
            }
        });

        replaceImg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Lang lang = srcLang;
                srcLang = tgtLang;
                tgtLang = lang;
                srcLangText.setText(srcLang.toString());
                tgtLangText.setText(tgtLang.toString());
            }
        });

        wordLiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTab(1);
            }
        });

        speechLiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTab(2);
            }
        });

    }

    //接收语种变化数据
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(LangMessage message) {
        if (message.isDirection()) {
            srcLang = message.getLang();
            srcLangText.setText(srcLang.toString());
        } else {
            tgtLang = message.getLang();
            tgtLangText.setText(tgtLang.toString());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void init() {

        fragments.add(new TextFragment());
        fragments.add(new SpeechFragment());

        fragmentStatePagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };

        viewPager.setAdapter(fragmentStatePagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                int currentItem = viewPager.getCurrentItem();
                setTab(currentItem + 1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        viewPager.setOffscreenPageLimit(1);//设置缓存左右临近的1个页面（共2个页面）
        //将第一个标签标记为选中
        setTab(1);

    }

    public void selectTab(int i) {

        setTab(i);
        viewPager.setCurrentItem(i - 1);
    }


    public void setTab(int i) {
        resetTab();
        switch (i) {
            case 1:
                wordIcon.setImageResource(R.drawable.text_icon);
                wordText.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            case 2:
                speeehIcon.setImageResource(R.drawable.speech_icon);
                speechText.setTextColor(Color.parseColor("#FFFFFF"));
                break;
        }
    }


    public void resetTab() {
        wordIcon.setImageResource(R.drawable.text_icon);
        wordText.setTextColor(Color.parseColor("#F7B940"));
        speeehIcon.setImageResource(R.drawable.speech_icon);
        speechText.setTextColor(Color.parseColor("#F7B940"));
    }

}
