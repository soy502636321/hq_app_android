//package com.huiquan.ait.app.text;
//
//import android.content.Intent;
//import android.graphics.Color;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.huiquan.ait.app.R;
//import com.huiquan.ait.app.replace.MessageLeft;
//import com.huiquan.ait.app.replace.MessageRight;
//import com.huiquan.ait.app.replace.ReplaceActivity;
//import com.huiquan.ait.app.speech.SpeechFragment;
//import com.huiquan.ait.app.text.TextFragment;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//import org.greenrobot.eventbus.ThreadMode;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class MainActivity extends AppCompatActivity implements View.OnClickListener {
//
//    private TabLayout tabLayout;
//    private ViewPager viewPager;
//    private Map<String, Fragment> fragmentMap = new HashMap<>();
//    private TextView leftText;
//    private TextView rightText;
//    private ImageView replaceImg;
//    private LinearLayout leftLiner;
//    private LinearLayout rightLiner;
//
//    private LinearLayout wordLiner;
//    private LinearLayout speechLiner;
//    private ImageView wordIcon;
//    private ImageView speeehIcon;
//    private TextView wordText;
//    private TextView speechText;
//
//
//    public String left_text;
//    public String right_text;
//
//    private List<Fragment> fragments = new ArrayList<>();
//    private FragmentStatePagerAdapter fragmentStatePagerAdapter;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
////        fragmentMap.put("文本", TextFragment.newInstance());
////        fragmentMap.put("语音", SpeechFragment.newInstance());
////
////        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
//
//
//
////        viewPager.setAdapter(new AppFragmentPagerAdapter(getSupportFragmentManager(), fragmentMap));
////        tabLayout.setupWithViewPager(viewPager);
//        initView1();
//        init();
//
//
////        initView();
//    }
//
//    private void initView1() {
//        viewPager = (ViewPager) findViewById(R.id.viewPager);
//
//        leftLiner = (LinearLayout) findViewById(R.id.left_liner);
//        rightLiner = (LinearLayout) findViewById(R.id.right_liner);
//        replaceImg = (ImageView) findViewById(R.id.replaceImg);
//        leftText = (TextView) findViewById(R.id.left_text);
//        rightText = (TextView) findViewById(R.id.right_text);
//
//        wordLiner = (LinearLayout) findViewById(R.id.word_liner);
//        speechLiner = (LinearLayout) findViewById(R.id.speech_liner);
//        wordIcon = (ImageView) findViewById(R.id.word_icon);
//        speeehIcon = (ImageView) findViewById(R.id.speeeh_icon);
//        wordText = (TextView) findViewById(R.id.word_text);
//        speechText = (TextView) findViewById(R.id.speech_text);
//
//        leftLiner.setOnClickListener(this);
//        rightLiner.setOnClickListener(this);
//        replaceImg.setOnClickListener(this);
//        leftText.setOnClickListener(this);
//        rightText.setOnClickListener(this);
//        wordLiner.setOnClickListener(this);
//        speechLiner.setOnClickListener(this);
//    }
//
//    private void init() {
//
//        fragments.add(new TextFragment());
//        fragments.add(new SpeechFragment());
//
//        fragmentStatePagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()){
//            @Override
//            public int getCount() {
//                return fragments.size();
//            }
//
//            @Override
//            public Fragment getItem(int position) {
//                return fragments.get(position);
//            }
//        };
//
//        viewPager.setAdapter(fragmentStatePagerAdapter);
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                int currentItem = viewPager.getCurrentItem();
//                setTab(currentItem + 1);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//            }
//        });
//
//        viewPager.setOffscreenPageLimit(1);//设置缓存左右临近的3个页面（共4个页面）
//        //将第一个标签标记为选中
//        setTab(1);
//
//    }
//
//    public void selectTab(int i) {
//
//        setTab(i);
//        viewPager.setCurrentItem(i - 1);
//    }
//
//
//    public void setTab(int i) {
//        resetTab();
//        switch (i) {
//            case 1:
//                wordIcon.setImageResource(R.drawable.text_icon);
//                wordText.setTextColor(Color.parseColor("#FFFFFF"));
//                break;
//            case 2:
//                speeehIcon.setImageResource(R.drawable.speech_icon);
//                speechText.setTextColor(Color.parseColor("#FFFFFF"));
//                break;
//        }
//    }
//
//
//    public void resetTab() {
//        wordIcon.setImageResource(R.drawable.text_icon);
//        wordText.setTextColor(Color.parseColor("#F7B940"));
//        speeehIcon.setImageResource(R.drawable.speech_icon);
//        speechText.setTextColor(Color.parseColor("#F7B940"));
//    }
//
//
//    private void initView() {
//        leftLiner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent left_text = new Intent(MainActivity.this, ReplaceActivity.class);
//                left_text.putExtra("other", "0");
//                startActivity(left_text);
//            }
//        });
//        rightLiner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent right_text = new Intent(MainActivity.this, ReplaceActivity.class);
//                right_text.putExtra("other", "1");
//                startActivity(right_text);
//            }
//        });
//
//        replaceImg.setOnClickListener(new View.OnClickListener() {
//            public String left_text;
//            public String right_text;
//
//            @Override
//            public void onClick(View view) {
//                left_text = leftText.getText().toString();
//                right_text = rightText.getText().toString();
//                leftText.setText(right_text);
//                rightText.setText(left_text);
//            }
//        });
//    }
//
//    //接收左边的数据
//    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
//    public void onMessageEvent(MessageLeft messageLeft) {
//        leftText.setText(messageLeft.getMessage());
//    }
//
//    //接收右边的数据
//    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
//    public void onMessageEvent(MessageRight messageRight) {
//        rightText.setText(messageRight.getMessage());
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        EventBus.getDefault().unregister(this);
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.word_liner:
//                selectTab(1);
//                break;
//
//            case R.id.speech_liner:
//                selectTab(2);
//                break;
//
//            case R.id.left_liner:
//                Intent left_liner = new Intent(MainActivity.this, ReplaceActivity.class);
//                left_liner.putExtra("other", "0");
//                startActivity(left_liner);
//                break;
//
//            case R.id.right_liner:
//                Intent right_liner = new Intent(MainActivity.this, ReplaceActivity.class);
//                right_liner.putExtra("other", "1");
//                startActivity(right_liner);
//                break;
//
//            case R.id.replaceImg:
//                left_text = leftText.getText().toString();
//                right_text = rightText.getText().toString();
//                leftText.setText(right_text);
//                rightText.setText(left_text);
//                break;
//
//        }
//    }
//}
