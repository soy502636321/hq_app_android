package com.huiquan.ait.app.speech;

/**
 * Created by soy50 on 2017/11/12.
 */
public class Chat {
    private String srcText;
    private String tgtText;
    private boolean direction;

    public Chat() {

    }

    public Chat(String srcText, String tgtText, boolean direction) {
        this.srcText = srcText;
        this.tgtText = tgtText;
        this.direction = direction;
    }

    public String getTgtText() {
        return tgtText;
    }

    public String getSrcText() {
        return srcText;
    }

    public void setTgtText(String tgtText) {
        this.tgtText = tgtText;
    }

    public void setSrcText(String srcText) {
        this.srcText = srcText;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public boolean isDirection() {
        return direction;
    }
}
