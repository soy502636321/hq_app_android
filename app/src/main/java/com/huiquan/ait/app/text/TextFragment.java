package com.huiquan.ait.app.text;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huiquan.ait.app.MainActivity;
import com.huiquan.ait.app.R;

import com.huiquan.ait.app.lang.LangMessage;
import okhttp3.*;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class TextFragment extends Fragment {
    private TextView translateResult;
    private ListView translateExample;
    private EditText translateText;
    private List<Example> examples = new ArrayList<>();
    private ExampleAdapter exampleAdapter;
    private ImageView translate_sound;
    private boolean isClick = true;

    public static TextFragment newInstance() {
        TextFragment fragment = new TextFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text, container, false);
        initView();
        translateText = view.findViewById(R.id.translate_text);
        setTranslateResult((TextView) view.findViewById(R.id.translate_result));
        setTranslateExample((ListView) view.findViewById(R.id.translate_example));
        exampleAdapter = new ExampleAdapter(examples, getContext());
        getTranslateExample().setAdapter(exampleAdapter);

        view.findViewById(R.id.translate_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeInput();
                translator();
                example();
            }
        });

        translateText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    closeInput();
                    translator();
                    example();
                }
                return false;
            }
        });
        return view;
    }

    //样例监听
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onExampleEvent(List<Example> examples) {
        getExamples().clear();
        getExamples().addAll(examples);
        getExampleAdapter().notifyDataSetChanged();
    }

    //翻译监听
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onTranslatorEvent(Translator translator) {
        translateResult.setText(Html.fromHtml(translator.getTgtText()));
    }

    private void closeInput() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(translateText.getWindowToken(), 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public TextView getTranslateResult() {
        return translateResult;
    }

    public void setTranslateResult(TextView translateResult) {
        this.translateResult = translateResult;
    }

    public ListView getTranslateExample() {
        return translateExample;
    }

    public void setTranslateExample(ListView translateExample) {
        this.translateExample = translateExample;
    }

    public ExampleAdapter getExampleAdapter() {
        return exampleAdapter;
    }

    public void setExampleAdapter(ExampleAdapter exampleAdapter) {
        this.exampleAdapter = exampleAdapter;
    }

    public List<Example> getExamples() {
        return examples;
    }

    public void setExamples(List<Example> examples) {
        this.examples = examples;
    }

    private void initView() {
//        translate_sound = (ImageView) getActivity().findViewById(R.id.translate_sound);
//        translate_sound.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isClick == true) {
//                    isClick = false;
//                    Glide.with(getActivity()).load(R.drawable.sound).asGif().into(translate_sound);
//                } else {
//                    isClick = true;
//                    Glide.with(getActivity()).load(R.mipmap.sound).into(translate_sound);
//                }
//
//            }
//        });
    }

    private void example() {
        String text = translateText.getText().toString();
        OkHttpClient okHttpClient = new OkHttpClient();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("src", MainActivity.srcLang.getCode());
        params.put("tgt", MainActivity.tgtLang.getCode());
        params.put("text", text);
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), new JSONObject(params).toString());
        Request request = new Request.Builder().url("http://ait.huiquan.com/app/api/example")
                .post(body)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                try {
                    JSONArray array = new JSONArray(response.body().string());
                    List<Example> examples = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject json = array.getJSONObject(i);
                        JSONObject variant = json.getJSONObject("variant");
                        Example example = new Example();
                        example.setSrcText(variant.getString(MainActivity.srcLang.getCode()));
                        example.setTgtText(variant.getString(MainActivity.tgtLang.getCode()));
                        examples.add(example);
                    }
                    EventBus.getDefault().post(examples);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void translator() {
        final String text = translateText.getText().toString();
        OkHttpClient okHttpClient = new OkHttpClient();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("src", MainActivity.srcLang.getCode());
        params.put("tgt", MainActivity.tgtLang.getCode());
        params.put("text", text);
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), new JSONObject(params).toString());
        final Request request = new Request.Builder().url("http://ait.huiquan.com/app/api/translator")
                .post(body)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                try {
                    JSONObject json = new JSONObject(response.body().string());
                    String result = json.getString("result");
                    EventBus.getDefault().post(new Translator(text, result));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
