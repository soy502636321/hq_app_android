package com.huiquan.ait.app.text;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.huiquan.ait.app.R;

import java.util.List;

/**
 * Created by soy50 on 2017/11/12.
 */
public class ExampleAdapter extends BaseAdapter {
    private List<Example> examples;
    private LayoutInflater inflater;

    public ExampleAdapter(List<Example> examples, Context context) {
        this.examples = examples;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return examples != null ? examples.size() : 0;
    }

    @Override
    public Example getItem(int position) {
        return (examples != null && position <= examples.size()) ? examples.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View exampleItem = inflater.inflate(R.layout.example_item, null);
        Example example = getItem(position);
        TextView exampleSrcText = exampleItem.findViewById(R.id.example_src_text);
        TextView exampleTgtText = exampleItem.findViewById(R.id.example_tgt_text);
        exampleSrcText.setText(example.getSrcText());
        exampleTgtText.setText(example.getTgtText());

        return exampleItem;
    }
}
