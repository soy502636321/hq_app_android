package com.huiquan.ait.app.text;

import java.util.Date;

/**
 * Created by soy50 on 2017/11/12.
 */
public class Example {
    private String id;
    private String srcText;
    private String tgtText;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getSrcText() {
        return srcText;
    }

    public void setSrcText(String srcText) {
        this.srcText = srcText;
    }

    public String getTgtText() {
        return tgtText;
    }

    public void setTgtText(String tgtText) {
        this.tgtText = tgtText;
    }
}
