package com.huiquan.ait.app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.huiquan.ait.app.speech.SpeechFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by soy50 on 2017/11/12.
 */
public class AppFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<String> names = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();

    public AppFragmentPagerAdapter(FragmentManager fm, Map<String, Fragment> fragmentMap) {
        super(fm);
        if (fragmentMap != null && fragmentMap.size() > 0) {
            this.names.addAll(fragmentMap.keySet());
            this.fragments.addAll(fragmentMap.values());
        }
    }

    @Override
    public Fragment getItem(int position) {
        return position <= fragments.size() ? fragments.get(position) : null;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return position <= names.size() ? names.get(position) : null;
    }
}
